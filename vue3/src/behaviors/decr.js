// decr.js
export default function (count) {
    function handleClick() {
        console.log("decr "+count)
        count.value--
    }

    return {
        handleClick
    }
}