export default {
    props: ['value'],
    data() {
        return {
            count: 0
        }
    },
    mounted() {
        console.log("mount counter")
        this.count = this.value ?? 0
    },
    methods: {
        handleClickInc() {
            console.log("inc");
            this.count++
        }
    }
}