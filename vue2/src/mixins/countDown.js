export default {
    props: ['value'],
    data() {
        return {
            count: 0
        }
    },
    mounted() {
        console.log("mount countdown")
        this.count = this.value ?? 0
    },
    methods: {
        handleClickDecr() {
            console.log("decr");
            this.count--
        }
    }
}