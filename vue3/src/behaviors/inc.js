// inc.js
export default function (count) {
    function handleClick() {
        console.log("inc "+count)
        count.value++
    }

    return {
        handleClick
    }
}